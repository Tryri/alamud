from .effect import Effect2
from mud.events import TalkEvent, TalkOfSmithEvent

class TalkEffect(Effect2):
    EVENT = TalkEvent

class TalkOfSmithEffect(Effect2):
    EVENT = TalkOfSmithEvent
