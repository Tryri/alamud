from .event import Event2

class TalkEvent(Event2):
    NAME = "talk"

    def perform(self):
        self.inform("talk")


class TalkOfSmithEvent(Event2):
    NAME = "talk-of-smith"

    def perform(self):
        self.inform("talk-of-smith")
